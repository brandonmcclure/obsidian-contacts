ifeq ($(OS),Windows_NT)
	SHELL := pwsh.exe
else
	SHELL := pwsh
endif

all: publish_dev

install_dependencies:
	npm install

build: install_dependencies
	npm run dev

publish_dev: build
	$$path = 'E:\Git\brandonmcclure\orgmode\.obsidian\plugins\obsidian-contacts'; Copy-Item .\main.js "$$Path\main.js" -Verbose; Copy-Item .\manifest.json "$$Path\manifest.json" -Verbose; Copy-Item .\styles.css "$$Path\styles.css" -Verbose