// import {readdirSync} from 'fs';
import * as path from 'path';
import { App, Editor, MarkdownView, Modal, Notice, View, FileExplorer ,Plugin, PluginSettingTab, Setting } from 'obsidian';
// Remember to rename these classes and interfaces!

interface obsidianContactsSettings {
	peopleFolder: string;
	ExistingContacts: ExistingContacts[];
}

export class ExistingContacts {
	notePath: string;
	Contact: string;
  
	constructor(notePath: string, Contact:string){
	  this.notePath = notePath;
	  this.Contact = Contact;
	}
  }

const DEFAULT_SETTINGS: obsidianContactsSettings = {
	peopleFolder: 'People',
	ExistingContacts: []
}

const rootHiddenClass = 'oz-root-hidden';
export default class obsidianContacts extends Plugin {
	fileExplorer?: FileExplorer;
	settings: obsidianContactsSettings;

	doHiddenRoot = (revert = false) => {
        if (!this.fileExplorer) {
            console.error('file-explorer not found');
            return;
        }
        const root = this.fileExplorer.fileItems[this.settings.peopleFolder];
		console.log(root.fileExplorer.fileItems.length);
        const styles = getComputedStyle(root.titleInnerEl);
        const setup = () => {
            const shouldHide =
                styles.display === 'none' ||
                styles.color === 'rgba(0, 0, 0, 0)';
            root.titleEl.toggleClass(rootHiddenClass, !revert && shouldHide);
        };
        if (styles.display !== '') setup();
        else {
            let count = 0;
            const doId = window.setInterval(() => {
                if (count > 10) {
                    console.error('%o styles empty', root.titleInnerEl);
                    window.clearInterval(doId);
                } else if (styles.display === '') {
                    count++;
                } else {
                    setup();
                    window.clearInterval(doId);
                }
            }, 100);
        }
    };

	getViewHandler = (revert: boolean) => (view: FileExplorer) => {
		this.fileExplorer = view;
		console.log("inside getting a view handler");
		this.doHiddenRoot(revert);
		if (!revert) {
			this.registerEvent(
				this.app.workspace.on('css-change', this.doHiddenRoot),
			);
			
};
	};
	async onload() {
		await this.loadSettings();
		console.log("getting a view handler");
		
		const fullPath = path.join(__dirname, this.settings.peopleFolder)
		const targetFolderName = path.basename(fullPath);
		console.log("targetFolderName: " + targetFolderName)
    	const folderEntries = readdirSync(fullPath);

		folderEntries.forEach(file => {
			console.log(file);
		  });

		console.log("this many files: " + folderEntries.length);
		// This creates an icon in the left ribbon.
		const ribbonIconEl = this.addRibbonIcon('info', 'Contacts', (evt: MouseEvent) => {
			// Called when the user clicks the icon.
			new Notice('People folder is '+this.settings.peopleFolder);
			new Notice('I found '+this.settings.ExistingContacts.length + ' people');
		});
		// Perform additional things with the ribbon
		ribbonIconEl.addClass('my-plugin-ribbon-class');

		// This adds a simple command that can be triggered anywhere
		this.addCommand({
			id: 'find-people-selection',
			name: 'Find all people in this selection',
			editorCallback: (editor: Editor) => {        
				const selectedText = editor.getSelection();
				// const onSubmit = (text: string, url: string) => {          
				// 	editor.replaceSelection(`[${text}](${url})`);        
				// };
				console.log("thing");    
			 }
		});

		// This adds a settings tab so the user can configure various aspects of the plugin
		this.addSettingTab(new SampleSettingTab(this.app, this));

		// If the plugin hooks up any global DOM events (on parts of the app that doesn't belong to this plugin)
		// Using this function will automatically remove the event listener when this plugin is disabled.
		this.registerDomEvent(document, 'click', (evt: MouseEvent) => {
			console.log('click', evt);
		});

		// When registering intervals, this function will automatically clear the interval when the plugin is disabled.
		this.registerInterval(window.setInterval(() => console.log('setInterval'), 5 * 60 * 1000));

		console.log("loaded obsidian-contacts");
	}

	onunload() {

	}

	async loadSettings() {
		this.settings = Object.assign({}, DEFAULT_SETTINGS, await this.loadData());
	}

	async saveSettings() {
		await this.saveData(this.settings);
	}
}

class SampleSettingTab extends PluginSettingTab {
	plugin: obsidianContacts;

	constructor(app: App, plugin: obsidianContacts) {
		super(app, plugin);
		this.plugin = plugin;
	}

	display(): void {
		const {containerEl} = this;

		containerEl.empty();

		containerEl.createEl('h2', {text: 'Settings for the obsidian contacts plugin'});

		new Setting(containerEl)
			.setName('People folder')
			.setDesc('The folder that notes for people/contacts will go into')
			.addText(text => text
				.setPlaceholder('People')
				.setValue(this.plugin.settings.peopleFolder)
				.onChange(async (value) => {
					console.log('peopleFolder: ' + value);
					this.plugin.settings.peopleFolder = value;
					await this.plugin.saveSettings();
				}));
	}
}
